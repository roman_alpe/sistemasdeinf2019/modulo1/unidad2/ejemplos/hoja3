﻿USE ciclistas;


/**.1-Consultas de Totales: (HOJA 3)
  Listar las edades de todos los ciclistas de Banesto.**/
  
  SELECT DISTINCT c.edad 
    FROM ciclista c
    WHERE c.nomequipo='Banesto';

/**.2- listar las edades de los ciclistas 
  que son de Banesto o de Navigare.**/
  SELECT DISTINCT c.edad 
    FROM ciclista c
    WHERE c.nomequipo='Banesto' 
    OR c.nomequipo='Navigare';   
/**Usando la claúsula IN **/
  
  SELECT DISTINCT c.edad 
    FROM ciclista c
    WHERE c.nomequipo 
    IN('Banesto','Navigare');
/**.3- Listar el dorsal de los ciclistas 
  que son de Banesto y cuya edad está entre 25 y 32**/
    SELECT DISTINCT c.dorsal 
    FROM ciclista c 
    WHERE c.nomequipo='Banesto' AND c.edad BETWEEN 25 AND 32;
/**.4- Listar el dorsal de los ciclistas 
  que son de Banesto y cuya edad está entre 25 ó 32**/
  SELECT DISTINCT c.dorsal 
    FROM ciclista c 
    WHERE c.nomequipo='Banesto' OR c.edad BETWEEN 25 AND 32;
/**.5- Listar la inicial del equipo de los ciclistas 
  cuyo nombre comience por R **/
  
  SELECT DISTINCT LEFT(c.nomequipo,1)  
    FROM ciclista c
    WHERE c.nombre LIKE 'R%';

  /**.6- Listar el código de las etapas 
  que su salida y llegada sea en la misma población.**/
    SELECT e.numetapa 
      FROM etapa e 
      WHERE e.salida=e.llegada;
  /**.7-Listar el código de las etapas 
      que su salida y llegada 
      no sean en la misma población 
      y que conozcamos el dorsal del ciclista 
      que ha ganado la etapa.**/
      SELECT e.numetapa 
        FROM etapa e
        WHERE e.salida <> e.llegada AND e.dorsal IS NOT NULL;
      
      
      /**.8- Listar el nombre de los puertos 
        cuya altura entre 1000 y 2000 
        o que la altura sea mayor que 2400.**/
           
      SELECT p.nompuerto 
        FROM puerto p
        WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura>2400;
      
     -- 9. Listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor de 2400

      SELECT 
        DISTINCT p.dorsal
      FROM 
        puerto p 
      WHERE 
        p.altura BETWEEN 1000 AND 2000
      UNION
      SELECT 
        DISTINCT p.dorsal 
      FROM 
        puerto p 
      WHERE 
        p.altura>2400;

     
     /**.10-listar el número de ciclistas 
        que hayan ganado alguna etapa**/
      SELECT COUNT(DISTINCT dorsal) nCiclistas
        FROM etapa e;
      /**Con Subconsulta**/
      SELECT COUNT(*) nCiclistas 
        FROM(          
      SELECT DISTINCT dorsal nCiclistas
        FROM etapa e) c1;
      
      /**.11- Listar el número de etapas 
        que tengan puerto.**/
      SELECT COUNT(DISTINCT p.numetapa) nEtapas
        FROM puerto p;
     
     /**.12-Listar el número de ciclistas 
        que hayan ganado algún puerto.**/
      SELECT COUNT(DISTINCT p.dorsal) nCiclistas 
        FROM puerto p;
     /**.13- Listar el código de la etapa 
        con el número de puertos que tiene **/
      SELECT p.numetapa, COUNT(*) 
        FROM puerto p 
        GROUP BY p.numetapa;

      
      
      /**.14- Indicar la altura media de los puertos**/
      SELECT AVG(p.altura)
        FROM puerto p;
      
      
      
      /**.15-Indicar el código de etapa 
        cuya altura media de sus puertos 
        esté por encima de 1500.**/
      SELECT p.numetapa
        FROM puerto p
        HAVING AVG(p.altura)>1500;
       
      /**Con GROUP BY**/
      SELECT p.numetapa, AVG(altura) altmedia 
        FROM puerto p
        GROUP BY p.numetapa;
      /**Con Subconsulta**/
      SELECT c1.numetapa 
       FROM( 
       SELECT p.numetapa, AVG(altura) altmedia 
        FROM puerto p
        GROUP BY p.numetapa) c1
        WHERE c1.altmedia>1500;
      
      /**.16-Indicar el número de etapas 
        que cumplen la condición anterior.**/
        
        SELECT p.numetapa 
          FROM puerto p
          GROUP BY p.numetapa
          HAVING AVG(p.altura)>1500;
        
        SELECT COUNT(*) nEtapas 
          FROM(
            SELECT p.numetapa 
              FROM puerto p
              GROUP BY p.numetapa
              HAVING AVG(p.altura)>1500) c1;
       
       /**.17- Listar el dorsal del ciclista 
        con el número de veces 
        que ha llevado algún maillot.**/
        SELECT l.dorsal, l.código, COUNT(*)
          FROM lleva l
          GROUP BY l.dorsal, l.código; 
        
        /**.18- Listar el dorsal del ciclista 
        con el çodigo del maillot 
        y cuantas veces ese ciclista 
        ha llevado ese maillot.**/
        
        SELECT l.dorsal,l.código, COUNT(*) numero
          FROM lleva l
          GROUP BY l.dorsal,l.código;
        
        /**.19- Listar el dorsal, el código de etapa, 
          el ciclista y el número de maillots 
          que ese ciclista ha llevado en cada etapa.**/
          
          SELECT l.dorsal,l.código 
            FROM lleva l
            GROUP BY l.dorsal,l.numetapa;
          
          SELECT c1.dorsal
            FROM(
              SELECT l.dorsal,l.código 
                FROM lleva l
                GROUP BY l.dorsal,l.numetapa) c1
          INNER JOIN
            ciclista c
          ON c1.dorsal=c.dorsal;

          SELECT l.dorsal,l.numetapa, c.nombre, COUNT(*) numero
             FROM lleva l
             JOIN ciclista c
              ON l.dorsal = c.dorsal
             GROUP BY c.dorsal,l.numetapa;